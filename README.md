# jtone (AKA multitone)
---
#### Author: Paolo Giachin (jacoisback@gmail.com) [Website](http://www.lemie5lire.com)
#### Repo: https://bitbucket.org/jacoisback/jtone
#### Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)
---

## Introduction
**jtone** is a cli waveform generator with this set of features:

   - Multiple waveforms
   - Multiple tones types
   - Smooth start/end of genrated data
   - pulse and burst
   - dat and wav file format

The output can be either the console or a file.
Currently supported format are .dat and .wav file.

## What it isn't
As I mentioned this program is ment to generate a data file, it's not an audio tool or a signal processing tool, and it will never be something like that.

## Setup
You can clone jtone from bitbucket repo and it will be ready to be compiled.
You need make and g++ (and the cpp gcc/g++ build toolchain)
Launching make all from the project root folder will create two executable named "jtone" one in **Release** folder and one in **Debug**
The first one is optimized for running, the second one include debug symbols to be used with gdb.

### *Important note*
*The first time you clone the project, you need to create an obj folder in both Release and Debug folder or compilation will not work! Sorry, I had no time yet to fix this.*

### pushing changes.
Before pushing any commit, you have to raun the test.sh script for a minimum functional test. This will be automaically run on any cmmitted change.


## Bug reporting
You can report bug, issues or request features here: [website]()https://bitbucket.org/jacoisback/jtone/issues/new)

## Documentation
jtone is quite simple ant the -h parameter provide a simple help that will let you run the program without issues.
There are few comments in the code that explain main lines but, yea, not enough.
So, if you like you can contribute. This will be really appreciated.

## Play with it
On linux system using ALSA you can play your tone with the following commands:
```
jtone [params] -O out.wav
aplay out.wav
```
The following example will generate a sinlge tone sine signal @1kHz with a 30dB attenuation of the duration of 2 seconds @48000 bit/s sample rate with a smooth time at start and and of 0.5 seconds.  
Then it's converted in a raw 16 bit data file (using the same bitrate) with sox and finally aplay will make it sounding on the first device installed on PC at same bitrate wit the same resolution.

```
jtone -m t -w s -g -30  -r 48000   -t 2 -f 1000 -s 0.5 -O out.wav; aplay out.wav -D hw:CARD=0,0
```

---
### License and copyright.
---
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

See gpl.txt for details and [website](http://www.gnu.org/licenses/)
