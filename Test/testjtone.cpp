/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <iomanip>

int CheckValue(const char * descr, double value, char *linf, char *lsup, bool IsInteger)
{
   double dlinf=atof(linf);
   double dlsup=atof(lsup);
   string limits;
   limits.append(" [ ");
   limits.append(linf);
   limits.append(" .. ");
   limits.append(lsup);
   limits.append(" ] ");
   limits = (dlinf==0&&dlsup==0) ? "":limits;

   int result=(dlinf!=dlsup &&(value<dlinf || value > dlsup));  //Faiil if out of limitis when limits are different
   char *resOK= (dlinf==0&&dlsup==0) ? (char *)"":(char *)"PASS";
      cout <<setw(10) <<left << descr << right << fixed
      << setw(10) << setprecision (IsInteger ? 0:3) << value
      <<left << setw(25)<< limits
      << (result ? "FAIL" : resOK)
      <<endl;
   return result;
}

void fcout(const char * text, double value)
{
   value=abs(value)< 0.0005 ? 0:value;
   cout <<setw(10) <<left << text << right <<fixed  << setw(14) << setprecision (3) << value << endl;

}

int main(int argc, char *argv[]) {
ifstream infile("/dev/stdin");
double a=0, b,c,preva=0;
int cnt=0,peaks=0;
string line;
double dsum=0, dssum=0,dmin=0,dmax=0,val1=0,val2=0;
int failcount=0;
bool nolimits=false;
if (argc>1&& !strncmp(argv[1],"-v",2)) nolimits=true;
else if (argc<19)
{
   cout <<"all parametres are needed! Provided only " << right << argc <<endl;
   return -1;
}
while (getline(infile, line))
{
  preva=a;
    istringstream iss(line);
    if ((iss >> a >> b >> c)) {
	cnt++;
	dsum+=b;
	dssum+=b*b;
	dmax=max(b,dmax);
	dmin=min(b,dmin);
// pos peak
	if (val1 <=val2 && b <val2) peaks++;
// neg peak
	if (val1 >=val2 && b >val2) peaks++;
	val1=val2;
	val2=b;

	}
}
if (!nolimits)
{
  failcount+=CheckValue("AVG", dsum/cnt,                    argv[1],argv[2],false);
  failcount+=CheckValue("RMS", sqrt(dssum/cnt),             argv[3],argv[4],false);
  failcount+=CheckValue("RMS_DB",20*log10(sqrt(dssum/cnt)),      argv[5],argv[6],false);
  failcount+=CheckValue("SAMPLES",cnt,                           argv[7],argv[8],true);
  failcount+=CheckValue("MAX",dmax,                               argv[9],argv[10],false);
  failcount+=CheckValue("MIN",dmin,                              argv[11],argv[12],false);
  failcount+=CheckValue("PEAKS",peaks-1,                         argv[13],argv[14],true);
  failcount+=CheckValue("DURATION",2*a-preva,                         argv[15],argv[16],false);
  failcount+=CheckValue("CREST", max(abs(dmax),abs(dmin))/sqrt(dssum/cnt), argv[17],argv[18],false);
}
else
{
   fcout("AVG", dsum/cnt);
   fcout("RMS",sqrt(dssum/cnt));
   fcout("RMS_DB", 20*log10(sqrt(dssum/cnt)));
   fcout("SAMPLES" , cnt);
   fcout("MAX",dmax);
   fcout("MIN" ,dmin);
   fcout("PEAKS" , peaks-1);
   fcout("DURATION" , 2*a-preva);
   fcout("CREST" , max(abs(dmax),abs(dmin))/sqrt(dssum/cnt));
}

return failcount;
}
