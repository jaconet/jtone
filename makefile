RM := rm -rf
CD := cd
MK := make

TARGETS := Release Debug



run:
	-$(CD) Release; $(MK)
	-$(CD) Debug; $(MK)
	-$(CD) Test; $(MK)

all:
	-$(CD) Release; $(MK) all
	-$(CD) Debug; $(MK) all
	-$(CD) Test; $(MK) all

clean:
	-$(CD) Release; $(MK) clean
	-$(CD) Debug; $(MK) clean
	-$(CD) Test; $(MK) clean



.PHONY: full all clean
