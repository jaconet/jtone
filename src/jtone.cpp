/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "include/build_defs.h"
#include "include/version.h"
#include "include/Generator.h"
#include <iostream>
#include <fstream>
using namespace std;
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define NEW_GEN

int main(int argc, char *argv[]) {
	Generator gen("/dev/stdout");

	if (gen.Init(argc,argv)) return 1;
  if (gen.convert) gen.ConvertFile();
	else if (gen.showHeader) gen.Wav->getInfo();
	else gen.Run();
	return 0;
}
