/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
using namespace std;
#include <string.h>
#include "Wave.h"
#include "Dat.h"
#ifndef GENERATOR_H

//Needed to compile also on cygwin
#define GENERATOR_H
#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

//!Duration of fading in seconds
#define SMOOTH_LEN 0.1

enum waveform {WF_SINE,WF_SQUARE,WF_QSINE,WF_SAW,WF_TRIANGLE} ;
typedef enum waveform Wform;
enum gen_modes {GM_SINGLE,GM_MULTI,GM_NOISE,GM_SWEEP};
typedef enum gen_modes gmodes;
enum pulse_modes {PM_SINGLE,PM_CONT,PM_REP};
typedef enum pulse_modes pmodes;


class Generator {
public:
   Generator(void);
   Generator(string output_stream);
   ~Generator(void);
   void Run(void);
   int Init(int argc, char **argv);
   WaveFile *Wav;
   DatFile *Dat;


   int ConvertFile(void);
   bool convert;
   int ConvertDatToWave(void);
   int ConvertWaveToDat(void);
   bool showHeader;


protected:
   double  Minfreq;
   double  Maxfreq;
   double  Div;
   double  Sampling;
   double  Duration;
   double  Amplitude;
   double  SoftStart;
   int Channels;
   int Bits;
   Wform Waveform;
   gmodes Mode;
   pmodes Pulse;
   double PulseLen;
   double PulsePauseLen;
   const char *sChannels[3]={"Left","Right","Left+Right"};
   double DoGain(double value);
   double DoPulse(double value, double curTime);

   double  CurrentValue(double  AbsoluteTime,double  Frequency,Wform waveform);
   void WriteHeader(void);
   int SetFrequencies(double  *frequencies, double  minfreq,double  maxfreq,double  div);
   void ShowHelp(void);
};




#endif //GENERATOR_H
