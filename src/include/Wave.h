/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <fstream>
#include <string>
#include <fstream>
#include <cstdint>
#include <string.h>

using namespace std;


#ifndef WAVE_H

#define WAVE_H



class WaveFile
{


public:
   typedef struct  WAV_HEADER
   {
      /* RIFF Chunk Descriptor */
      uint8_t         RIFF[4];        // RIFF Header Magic header
      uint32_t        ChunkSize;      // RIFF Chunk Size
      uint8_t         WAVE[4];        // WAVE Header
      /* "fmt" sub-chunk */
      uint8_t         fmt[4];         // FMT header
      uint32_t        Subchunk1Size;  // Size of the fmt chunk
      uint16_t        AudioFormat;    // Audio format 1=PCM,6=mulaw,7=alaw,     257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
      uint16_t        NumOfChan;      // Number of channels 1=Mono 2=Sterio
      uint32_t        SamplesPerSec;  // Sampling Frequency in Hz
      uint32_t        bytesPerSec;    // bytes per second
      uint16_t        blockAlign;     // 2=16-bit mono, 4=16-bit stereo
      uint16_t        bitsPerSample;  // Number of bits per sample
      /* "data" sub-chunk */
      uint8_t         Subchunk2ID[4]; // "data"  string
      uint32_t        Subchunk2Size;  // Sampled data length
   } wav_hdr;
   enum openmode {WF_READ,WF_WRITE};

   void getInfo();
   WaveFile(void);
   int OpenO(string wavefile);
   int OpenI(string wavefile);
   void CloseO(void);
   void CloseI(void);
   void SetHeader(uint16_t Channels,uint32_t Bitrate,uint32_t Bits, uint32_t Samples);
   void WriteSampleData(double value);
   void WriteSampleData(double value1,double value2);
   wav_hdr wav_whdr;
   fstream wavoFile;
   fstream waviFile;
private:
   string iwavefilename,owavefilename;
   openmode wavemode;
   void ShowHeader(wav_hdr *wavHeader);
   uint32_t wav_maxval;

};

#endif //WAVE_H
