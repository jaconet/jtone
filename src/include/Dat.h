/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <string.h>

using namespace std;


#ifndef DAT_H
#define DAT_H

class DatFile
{
   typedef struct DAT_HEADER
   {
      int Channels;
      uint32_t Bitrate;
      uint32_t Bits;
      uint32_t Samples;

   } dat_hdr;
public:
   enum openmode {DF_READ,DF_WRITE};
   void getInfo();
   DatFile(void);
   int OpenO(string datfile);
   int OpenI(string datfile);
   void CloseO(void);
   void CloseI(void);
   void SetHeader(uint16_t Channels,uint32_t Bitrate,uint32_t Bits, uint32_t Samples);
   void WriteSampleData(double current_time, double current_value);
   void WriteSampleData(double current_time, double current_value1, double current_value2);
   dat_hdr dat_dhdr;
   fstream datoFile;
   fstream datiFile;
private:
   string odatfilename,idatfilename;
   openmode datmode;
   void ShowHeader(dat_hdr *datHeader);



};



#endif //DAT_H
