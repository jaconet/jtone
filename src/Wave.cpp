/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <string.h>
#include "include/Wave.h"
#include <math.h>
using namespace std;



void WaveFile::SetHeader(uint16_t Channels,uint32_t Bitrate,uint32_t Bits, uint32_t Samples)
{
   memcpy(this->wav_whdr.RIFF,"RIFF",4);
   this->wav_whdr.ChunkSize=Channels*(Bits>>3)*Samples+sizeof(wav_hdr)-8;
   memcpy(this->wav_whdr.WAVE,"WAVE",4);

   memcpy(this->wav_whdr.fmt,"fmt ",4);
   this->wav_whdr.Subchunk1Size=16;
   this->wav_whdr.AudioFormat=1;
   this->wav_whdr.NumOfChan=Channels;
   this->wav_whdr.SamplesPerSec=Bitrate;
   this->wav_whdr.bytesPerSec=Channels*(((Bits-1)>>3)+1)*Bitrate;
   this->wav_whdr.blockAlign=Channels*(((Bits-1)>>3)+1);
   this->wav_whdr.bitsPerSample=Bits;
   memcpy(this->wav_whdr.Subchunk2ID,"data",4);
   this->wav_whdr.Subchunk2Size=Channels*(((Bits-1)>>3)+1)*Samples;
   this->wavoFile.seekg(0, ios::beg);
   this->wavoFile.write((char *)&this->wav_whdr,(streampos) sizeof(wav_hdr));
   this->wav_maxval=pow(2,(Bits-1))-1;

#ifdef WAVE_STDALONE
   this->ShowHeader(&this->wav_whdr);
#endif
}

void WaveFile::WriteSampleData(double  curval1,double curval2)
{
   int wav_size=this->wav_whdr.blockAlign/this->wav_whdr.NumOfChan;
   char wav_bfr[10];
   uint32_t wav_value1=(uint32_t)(curval1*(double)wav_maxval+(curval1<0?-0.5:0.5));
   uint32_t wav_value2=(uint32_t)(curval2*(double)wav_maxval+(curval2<0?-0.5:0.5));
   wav_bfr[0]=wav_value1;
   wav_bfr[1]=wav_value1>>8;
   wav_bfr[2]=wav_value1>>16;
   wav_bfr[3]=wav_value1>>24;
   wav_bfr[4]=wav_value2;
   wav_bfr[5]=wav_value2>>8;
   wav_bfr[6]=wav_value2>>16;
   wav_bfr[7]=wav_value2>>24;
   this->wavoFile.write(&wav_bfr[0],wav_size);
   if (this->wav_whdr.NumOfChan==2) this->wavoFile.write(&wav_bfr[4],wav_size);
}
void WaveFile::WriteSampleData(double  curval)
{
   this->WriteSampleData(curval,curval);
}




void WaveFile::ShowHeader(wav_hdr *wavHeader)
{
   cout << "RIFF header                :" << wavHeader->RIFF[0] << wavHeader->RIFF[1] << wavHeader->RIFF[2] << wavHeader->RIFF[3] << endl;
   cout << "Data size                  :" << wavHeader->ChunkSize << endl;
   cout << "WAVE header                :" << wavHeader->WAVE[0] << wavHeader->WAVE[1] << wavHeader->WAVE[2] << wavHeader->WAVE[3] << endl;
   cout << "FMT                        :" << wavHeader->fmt[0] << wavHeader->fmt[1] << wavHeader->fmt[2] << wavHeader->fmt[3] << endl;
   cout << "Subchunk1Size              :" << wavHeader->Subchunk1Size <<endl;
   // Display the sampling Rate from the header
   cout << "Audio Format               :" << wavHeader->AudioFormat << endl;
   cout << "Number of channels         :" << wavHeader->NumOfChan << endl;
   cout << "Sampling Rate              :" << wavHeader->SamplesPerSec << endl;
   cout << "Number of bytes per second :" << wavHeader->bytesPerSec << endl;
   cout << "Block align                :" << wavHeader->blockAlign << endl;
   cout << "Number of bits used        :" << wavHeader->bitsPerSample << endl;

   cout << "Data string                :" << wavHeader->Subchunk2ID[0] << wavHeader->Subchunk2ID[1] << wavHeader->Subchunk2ID[2] << wavHeader->Subchunk2ID[3] << endl;
   cout << "Subchunk2Size              :" << wavHeader->Subchunk2Size <<endl;
   cout << endl;

}


int WaveFile::OpenO(string wavefile)
{
   this->owavefilename=wavefile;
   this->wavoFile.open   (wavefile,ios::out|ios::binary);
   if (!this->wavoFile.is_open())
   {
       cout <<"Unable to open wave file: " << wavefile<<endl;
       return -1;
   }
   return 0;
}

int WaveFile::OpenI(string wavefile)
{
   this->iwavefilename=wavefile;
   this->waviFile.open(wavefile,ios::in|ios::binary);
   if (!this->waviFile.is_open())
   {
       cout <<"Unable to open wave file: " << wavefile<<endl;
       return -1;
   }
   return 0;
}


void WaveFile::CloseO(void)
{
   if (this->wavoFile.is_open()) this->wavoFile.close();
}

void WaveFile::CloseI(void)
{
   if (this->waviFile.is_open()) this->waviFile.close();
}


WaveFile::WaveFile(void)
{
}

void WaveFile::getInfo()
{
   wav_hdr wavHeader;
   int headerSize = sizeof(wav_hdr);
   streampos  filelength = 0;
      if (!this->waviFile.is_open()) return;
   //Read the header
   this->waviFile.seekg(0, ios::beg);
   this->waviFile.read((char *)&wavHeader,(streampos) headerSize);
   cout << "Header Read " << this->waviFile.tellg() << " bytes. "<< headerSize<< endl;
   if (this->waviFile)
   {
      this->waviFile.seekg(0, ios::end);
       filelength=this->waviFile.tellg();
       cout << "File is                    :" << filelength << " bytes." << endl;
       this->ShowHeader(&wavHeader);
   }
}

//#define WAVE_STDALONE
#ifdef WAVE_STDALONE
int main(int argc, char* argv[])
{

    string input;
    if (argc <= 1)
    {
        cout << "You need to provide a file name";
    }
    else
    {
        WaveFile *wf=new WaveFile();
        wf->OpenI(argv[1]);
        // wf->Open("pippo.wav",WF_READ);
        wf->getInfo();
        wf->CloseI();
        wf->OpenO("o.wav");
        wf->getInfo();
        wf->CloseO();
        // wf->Open("pippo.wav",WF_WRITE);
        // wf->SetHeader(2,48000,24, 48000*3);
        // wf->Close();
    }
    return 0;
}
#endif
