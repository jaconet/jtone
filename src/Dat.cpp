/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <cstdint>
#include <string.h>
#include "include/Dat.h"
#include "include/version.h"
#include <math.h>
using namespace std;



DatFile::DatFile(void)
{
}


int DatFile::OpenO(string datfile)
{
   this->odatfilename=datfile;
   this->datoFile.open(datfile,ios::out);
   if (!this->datoFile.is_open())
   {
       cout <<"Unable to open wave file: " << datfile<<endl;
       return -1;
   }
   return 0;
}

int DatFile::OpenI(string datfile)
{
   this->idatfilename=datfile;
   this->datiFile.open(datfile,ios::in);
   if (!this->datiFile.is_open())
   {
       cout <<"Unable to open wave file: " << datfile<<endl;
       return -1;
   }
   return 0;
}



void DatFile::getInfo()
{
}



void DatFile::CloseO(void)
{
   if (this->datoFile.is_open()) this->datoFile.close();
}
void DatFile::CloseI(void)
{
   if (this->datiFile.is_open()) this->datiFile.close();
}


void DatFile::SetHeader(uint16_t Channels,uint32_t Bitrate,uint32_t Bits, uint32_t Samples)
{
   this->dat_dhdr.Channels=Channels;
   this->dat_dhdr.Bitrate=Bitrate;
   this->dat_dhdr.Bits=Bits;
   this->dat_dhdr.Samples=Samples;

   this->datoFile << "; Sample Rate "   << Bitrate <<endl;
   this->datoFile << "; Channels 2 "    << Channels << endl;
   this->datoFile << "; Samples "      << Samples<<endl;
   this->datoFile << ";"<<endl;
   this->datoFile << "; Author http://www.jaconet.org "<<endl;
   this->datoFile << "; jtone rel.: "<< completeVersion <<endl;
   this->datoFile << ";"<<endl;
   this->datoFile << ";"<<endl;
   this->datoFile << ";"<<endl;

}


void DatFile::WriteSampleData(double curtime,double curval1,double curval2)
{
char buffer[200];
if (this->dat_dhdr.Channels==2) sprintf(buffer," %15.15f %15.15f %15.15f\n",curtime, curval1,curval2);  //printout data used by sox
else if (this->dat_dhdr.Channels==1) sprintf(buffer," %15.15f %15.15f %15.15f\n",curtime, 0.0,curval2);  //printout data used by sox
else sprintf(buffer," %15.15f %15.15f %15.15f\n",curtime, curval1,0.0);  //printout data used by sox
this->datoFile.write(&buffer[0],strlen(buffer));
}
void DatFile::WriteSampleData(double curtime,double curval)
{
   this->WriteSampleData( curtime, curval,curval);
}

void DatFile::ShowHeader(dat_hdr *datHeader)
{
}
