/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
using namespace std;
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "include/Generator.h"
#include "include/version.h"
#include "include/Wave.h"
#include "include/Dat.h"
double M2_PI=2*M_PI;

Generator::Generator(string output_stream)
{
   //Set default values
   this->Minfreq=20;    // Minimum freq in Hz, this is the frequency used for single tone genaration
   this->Maxfreq=20000; // Max freq in HZ (For jtone and Sweep)
   this->Div=3;         // Number of frequency per octave for jtone
   this->Sampling=48000;//Sampling rate
   this->Duration=2;    // Duration of tone in seconds excluding smooth-start and smooth-end time
   this->Amplitude=1;   // Level of signal will be used to multiplay the output.
   this->SoftStart=0;//SMOOTH_LEN;   // Duration of smooth-start smooth-end in seconds
   this->Channels=2;    //Number of channel used: 0=left, 1=right, 2= both
   this->Waveform=WF_SINE; //waveform to be generated.
   this->Mode=GM_SINGLE;        //Tipe of signal: Single, Multi, Noise, Sweep
   this->Pulse=PM_CONT;
   this->PulseLen=100;
   this->PulsePauseLen=100;
   this->Bits=16;
   this->Wav=new WaveFile();
   this->Wav->OpenO("/dev/null");
   this->Dat=new DatFile();
   this->Dat->OpenO("/dev/stdout");
   this->convert=false;
   this->showHeader=false;
}


Generator::~Generator()
{
   this->Dat->CloseO();
   this->Dat->CloseI();
   this->Wav->CloseO();
   this->Wav->CloseI();
}

Generator::Generator(void)
{
   Generator("/dev/stdout");
}



int Generator::SetFrequencies(double  *frequencies, double  minfreq,double  maxfreq,double  div)
{
   double  freq=minfreq;
   double  n=0.0;
   int m=0;
   if (!div) maxfreq=minfreq;

   //Calculate frequencies
   do
   {
      freq=minfreq*pow(2,n);
      n=n+1.0/div;
      frequencies[m]=freq;
      m++;
   }
   while (freq<maxfreq && m<1000);
   return m;
}




double Generator::DoGain(double value)
{
   value=this->Amplitude*value;
   value=(value>1) ? 1:value;
   value=(value<-1) ? -1:value;
   return value;
}

double Generator::DoPulse(double value, double curTime)
{
   double BurstTime;
   curTime*=1000;
   switch (this->Pulse)
   {
      case PM_CONT: break;
      case PM_SINGLE:
         if (curTime>this->PulseLen) value=0;
         break;
      case PM_REP:
         BurstTime=fmod(curTime, (this->PulseLen+this->PulsePauseLen));
         if (BurstTime>this->PulseLen) value=0;
         break;
   }
return value;
}



/***
Run the generator
***/
void Generator::Run(void)
{
   double  frequencies[1000];

   int tot_frequencies;
   if (this->Mode==GM_SWEEP || this->Mode==GM_NOISE || this->Mode==GM_SINGLE || this->Div==0) tot_frequencies=1;
   else tot_frequencies=this->SetFrequencies(&frequencies[0],this->Minfreq,this->Maxfreq,this->Div);  //Calculate the number of frequencies to be generated;
   double  smooth= this->SoftStart>0 ?this->Sampling*this->SoftStart  : 0;
   double  loopsize=this->Duration*this->Sampling+2*smooth;  // If smooth is wanted, then the duration shall be increased

   int freq;
   double  curval;
   double  abstime=0,dTime=1/this->Sampling;
   double  counter=0;
   double  smoothg=0,dsm=1/(smooth+1);
   double  lcsize=loopsize-smooth; //Used to reduce the number of operations in the loop
   double  phi=0;
   double  dfPI=M2_PI/this->Sampling;
   double  sfreq=this->Minfreq;
   double  deltafreq=(this->Maxfreq-this->Minfreq)/loopsize;
   double  deltaf=dfPI*sfreq;
   long rand_mod=65536,rand_off=rand_mod/2;
   this->Dat->SetHeader(this->Channels,this->Sampling,this->Bits,loopsize);
   this->Wav->SetHeader((!this->Channels)?1:this->Channels,this->Sampling,this->Bits,loopsize);

   while (counter<loopsize)
   {

      //Calculating current value of signal
      curval=0;
      switch(this->Mode)
      {
         case GM_SINGLE:
            curval=this->CurrentValue(abstime,this->Minfreq,this->Waveform);
         break;
         case GM_SWEEP:
            curval=this->CurrentValue(phi,1,this->Waveform);
            phi+=deltaf;
            deltaf=sfreq/this->Sampling;
            if (counter >= smooth && counter <=lcsize) sfreq+=deltafreq;
         break;
         case GM_MULTI:
            for (freq=0;freq<tot_frequencies;freq++)  curval+=this->CurrentValue(abstime,frequencies[freq],this->Waveform);
            curval/=tot_frequencies;
         break;
         case GM_NOISE:
            curval=rand()%rand_mod-rand_off;
   		   curval=curval/rand_off;
         break;

      }


      //Adjusting levels
      if (counter < smooth) smoothg+=dsm;
      else
      {
         if (counter > lcsize) smoothg-=dsm;
         else smoothg=1;
      }
      curval*=smoothg;     //smotth data
      curval=this->DoGain(curval);
      curval=this->DoPulse(curval,abstime);

      // Writing data to files.
      this->Dat->WriteSampleData(abstime, curval);
      this->Wav->WriteSampleData(curval);

      // Update loop counters
      counter=counter+1;
      abstime+=dTime;
   }
   this->Wav->CloseO();

}


/***
Calculate the value of the signal based on absolute time from t0, the frequency of signal and the waveform to be generated.

***/


double  Generator::CurrentValue(double  AbsoluteTime,double  Frequency,Wform waveform)
{
   double  abstime2m=fmod(AbsoluteTime*Frequency,2.0);
   double  time_2pi,cur_level;
   time_2pi=AbsoluteTime*(M2_PI);
   switch (waveform) {
      case WF_SINE:
         cur_level=sin(Frequency*time_2pi);
      break;
      case WF_SQUARE:
         if (abstime2m > 1.0) cur_level=-1.0;
         else cur_level=1.0;
      break;
      case WF_SAW:
      cur_level=1-abstime2m;
      break;
      case WF_TRIANGLE:
      if (abstime2m < 1.0) cur_level=1-2*abstime2m;
      else cur_level=2*abstime2m-3;
      break;
      case WF_QSINE:
      if (abstime2m > 1.5) cur_level=-1;
      else {
         if (abstime2m > 1) cur_level=0;
         else {
            if (abstime2m > 0.5) cur_level=1;
            else cur_level=0;
         }
      }
      break;
      default:
      cur_level=0;
      break;
      }
   return cur_level;
}
