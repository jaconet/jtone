/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "include/Generator.h"
#include "include/version.h"
#include <iostream>
#include <fstream>
using namespace std;
#include <math.h>
#include <string.h>
#include <stdlib.h>

void Generator::ShowHelp()
{
   cerr << "Generate audiofile (dat and/or wave)." << endl;
   cerr <<"Version: " << completeVersion<<endl;
   cerr << "Params:" << endl;
   cerr << "-a amplitude between 0 and 10 [1]\n\t--amplitude" << endl;
   cerr << "-b number of bits 2 to 32 [16]\n\t--bits" << endl;
   cerr << "-c channels: 0=left, 1=right, 2= both [2]\n\t--channels" << endl;
   cerr << "-C convert files\n\t--convert" << endl;
   cerr << "-d Number of frequency within an octave, from 1 to 12 [3]\n\t--division" << endl;
   cerr << "-f minimum frequency in the range of 1 to 96000Hz [20]\n\t--minfreq" << endl;
   cerr << "-F Maximum frequency in the range of 1 to 96000 Hz [20000]\n\t--maxfreq" << endl;
   cerr << "-g dB gain between -120 and +20 [~-1]\n\t--gain" << endl;
   cerr << "-h show this help\n\t--help" << endl;
   cerr << "-i input DAT file for conversion\n\t--indatfile" << endl;
   cerr << "-I input WAV file for conversion\n\t--inwavfile" << endl;
   cerr << "-m mode: t|singletone for single tone, m|multitone for multi tone, s|sweep for sweep, n|noise for noise [t]\n\t----mode" << endl;
   cerr << "-o output DAT file, [stdout]\n\t--datfile" << endl;
   cerr << "-O output WAV file [/dev/null]\n\t--wavfile" << endl;
   cerr << "-p pulse mode c|continuous for continuous (no burst), s|single for single, r|repeated for repeated. [c]\n\t--pulse" << endl;
   cerr << "-P pulse with in milliseconds 0 to 5000 [100]\n\t--pulsewith" << endl;
   cerr << "-Q pulse pause with in milliseconds 0 to 5000 [100]\n\t--pulsepause" << endl;
   cerr << "-r Samling rate, from 1000 to 192000 [48000](check audio card supported rates to avoid resampling)\n\t--bitraye" << endl;
   cerr << "-s smooth intro and exit, seconds from 0.01 to 10 [0]--smoothlen" << endl;
   cerr << "-t Duration in seconds from 0.01 to 600 [2]. You need to add 2*smooth time (-s parameter)\n\t--duration" << endl;
   cerr << "-v show version\n\t--version" << endl;
   cerr << "-w waveform: s|sine for sine, q|square for square, t|trienge for triangle, w|sawtooth for saw tooth, x|quasisine for quasi-sine [s]\n\t--waveform" << endl;

}

int Generator::Init(int argc,char **argv)
{
   int i;
   for (i=1;i<argc;i++)
	{
		if (!strcmp("-f",argv[i])||!strcmp("--minfreq",argv[i]))
		{
			if (i<argc)
			{
				this->Minfreq=atof(argv[i+1]);
				i++;
				if (this->Minfreq<1 || this->Minfreq >96000)
				{
					cerr << "Invalid par value: Minfreq must be 1...96000"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-F",argv[i])||!strcmp("--maxfreq",argv[i]))
		{
			if (i<argc -1)
			{
				this->Maxfreq=atof(argv[i+1]);
				i++;
				if (this->Maxfreq<1 || Maxfreq >96000)
				{
					cerr << "Invalid par value: Maxfreq must be 1...96000"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-d",argv[i])||!strcmp("--division",argv[i]))
		{
			if (i<argc -1)
			{
				this->Div=atof(argv[i+1]);
				i++;
				if (this->Div<1 || this->Div >12)
				{
					cerr << "Invalid par value: Div must be 1...12"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-r",argv[i])||!strcmp("--bitrate",argv[i]))
		{
			if (i<argc -1)
			{
				this->Sampling=atof(argv[i+1]);
				i++;
				if (this->Sampling<1000 || this->Sampling >192000)
				{
					cerr << "Invalid par value: Sampling must be 1000...192000"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-t",argv[i])||!strcmp("--duration",argv[i]))
		{
			if (i<argc -1)
			{
				this->Duration=atof(argv[i+1]);
				i++;
				if (this->Duration<0.009 || this->Duration >600)
				{
					cerr << "Invalid par value: Duration must be 0.01...600"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-c",argv[i])||!strcmp("--channels",argv[i]))
		{
			if (i<argc -1)
			{
				this->Channels=atoi(argv[i+1]);
				i++;
				if (this->Channels<0 || this->Channels >2)
				{
					cerr << "Invalid par value: channels must be 0...2"<<endl;
					return 1;
				}
			}
		}
    else if (!strcmp("-b",argv[i])||!strcmp("--bits",argv[i]))
		{
			if (i<argc -1)
			{
				this->Bits=atoi(argv[i+1]);
				i++;
				if (this->Bits<2 || this->Bits >32)
				{
					cerr << "Invalid par value: channels must be 2...32"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-a",argv[i])||!strcmp("--amplitude",argv[i]))
		{
			if (i<argc -1)
			{
				this->Amplitude=atof(argv[i+1]);
				if (this->Amplitude<0 || this->Amplitude >10)
				{
					cerr << "Invalid par value: Aplitude must be 0...10"<<endl;
					return 1;
				}
				i++;
			}
		}
		else if (!strcmp("-g",argv[i])||!strcmp("--gain",argv[i]))
		{
			if (i<argc -1)
			{
				this->Amplitude=atof(argv[i+1]);
				this->Amplitude=exp(this->Amplitude/20*log(10));
				if (this->Amplitude<0 || this->Amplitude >10)
				{
					cerr << "Invalid par value: Gain must be -120...+20"<<endl;
					return 1;
				}
				i++;
			}
		}
		else if (!strcmp("-m",argv[i])||!strcmp("--mode",argv[i]))
		{
			if (i<argc -1)
			{
				if (!strcmp("t",argv[i+1])||!strcmp("singletone",argv[i+1])) this->Mode=GM_SINGLE;
				else if (!strcmp("m",argv[i+1])||!strcmp("multitone",argv[i+1])) this->Mode=GM_MULTI;
				else if (!strcmp("s",argv[i+1])||!strcmp("sweep",argv[i+1])) this->Mode=GM_SWEEP;
				else if (!strcmp("n",argv[i+1])||!strcmp("noise",argv[i+1])) this->Mode=GM_NOISE;
				else
				{
					cerr << "Invalid par value: mode must be m|t|s|n"<<endl;
					return 1;
				}
            i++;
			}
		}
      else if (!strcmp("-p",argv[i])||!strcmp("--pulse",argv[i]))
		{
			if (i<argc -1)
			{
				if (!strcmp("c",argv[i+1])||!strcmp("continuous",argv[i+1])) this->Pulse=PM_CONT;
				else if (!strcmp("s",argv[i+1])||!strcmp("single",argv[i+1])) this->Pulse=PM_SINGLE;
				else if (!strcmp("r",argv[i+1])||!strcmp("repeated",argv[i+1])) this->Pulse=PM_REP;
				else
				{
					cerr << "Invalid par value: pulse mode must be c|s|r"<<endl;
					return 1;
				}
            i++;
			}
		}
      else if (!strcmp("-P",argv[i])||!strcmp("--pulsewidth",argv[i]))
		{
			if (i<argc -1)
			{
				this->PulseLen=atof(argv[i+1]);
				if (this->PulseLen<0 || this->PulseLen >5000)
				{
					cerr << "Invalid par value: must be 0...5000"<<endl;
					return 1;
				}
				i++;
			}
		}
      else if (!strcmp("-Q",argv[i])||!strcmp("--pulsepause",argv[i]))
		{
			if (i<argc -1)
			{
				this->PulsePauseLen=atof(argv[i+1]);
				if (this->PulsePauseLen<0 || this->PulsePauseLen >5000)
				{
					cerr << "Invalid par value: must be 0...5000"<<endl;
					return 1;
				}
				i++;
			}
		}
		else if (!strcmp("-w",argv[i])||!strcmp("--waveform",argv[i]))
		{
			if (i<argc -1)
			{
				if (!strcmp("s",argv[i+1])||!strcmp("sine",argv[i+1])) this->Waveform=WF_SINE;
				else if (!strcmp("q",argv[i+1])||!strcmp("square",argv[i+1]))  this->Waveform=WF_SQUARE;
				else if (!strcmp("t",argv[i+1])||!strcmp("triangle",argv[i+1]))  this->Waveform=WF_TRIANGLE;
				else if (!strcmp("w",argv[i+1])||!strcmp("sawtooth",argv[i+1]))  this->Waveform=WF_SAW;
				else if (!strcmp("x",argv[i+1])||!strcmp("quasisine",argv[i+1]))  this->Waveform=WF_QSINE;
				else
				{
					cerr << "Invalid par value: mode must be s|q|t|w|x"<<endl;
					return 1;
				}
            i++;
			}
		}
		else if (!strcmp("-o",argv[i])||!strcmp("--datfile",argv[i]))
		{
			if (i<argc-1)
			{
				if (!strlen(argv[i+1]))
				{
					cerr << "Invalid par value: mode must be a valid file"<<endl;
					return 1;
				}
         this->Dat->CloseO();
		   if (this->Dat->OpenO(argv[i+1])) return 1;
			i++;
			}
		}
    else if (!strcmp("-O",argv[i])||!strcmp("--wavfile",argv[i]))
		{
			if (i<argc-1)
			{
				if (!strlen(argv[i+1]))
				{
					cerr << "Invalid par value: mode must be a valid file"<<endl;
					return 1;
				}
      this->Wav->CloseO();
      if (this->Wav->OpenO(argv[i+1])) return 1;
			i++;
			}
		}
      else if (!strcmp("-i",argv[i])||!strcmp("--indatfile",argv[i]))
		{
			if (i<argc-1)
			{
				if (!strlen(argv[i+1]))
				{
					cerr << "Invalid par value: mode must be a valid file"<<endl;
					return 1;
				}
         this->Dat->CloseI();
		   if (this->Dat->OpenI(argv[i+1])) return 1;
			i++;
			}
		}
      else if (!strcmp("-I",argv[i])||!strcmp("--inwavfile",argv[i]))
		{
			if (i<argc-1)
			{
				if (!strlen(argv[i+1]))
				{
					cerr << "Invalid par value: mode must be a valid file"<<endl;
					return 1;
				}
         this->Wav->CloseI();
		   if (this->Wav->OpenI(argv[i+1])) return 1;
			i++;
			}
		}

		else if (!strcmp("-s",argv[i])||!strcmp("--smoothlen",argv[i]))
		{
			SoftStart=SMOOTH_LEN;

			if (i<argc -1)
			{
				SoftStart=atof(argv[i+1]);
				i++;
				if (SoftStart<0.009 || SoftStart >10)
				{
					cerr << "Invalid par value: Duration must be 0.01...10"<<endl;
					return 1;
				}
			}
		}
		else if (!strcmp("-h",argv[i])||!strcmp("--help",argv[i]))
		{
         this->ShowHelp();
			return 1;
		}
      else if (!strcmp("-v",argv[i])||!strcmp("--version",argv[i]))
		{
         cerr <<"Version: " << completeVersion<<endl;
			return 1;
		}
    else if (!strcmp("-X",argv[i]))
  {
    this->showHeader=true;
  }
      else if (!strcmp("-C",argv[i])||!strcmp("--convert",argv[i]))
		{
         this->convert=true;
		}
      else
      {
         cerr << "Parameter " << argv[i] <<" is not valid!" <<endl;
         this->ShowHelp();
         return 1;
      }
	}
	if (this->Minfreq>this->Maxfreq)
	{
		cerr << "Must be Maxfreq >= Minfreq"<<endl;
		return 1;
	}
	if (this->Maxfreq>this->Sampling/2)
	{
		cerr << "maxfreq shall be less than sampling/2"<<endl;
		return 1;
	}

return 0;
}
