/*
jtone
Copyright (C) 2017-2018  Paolo Giachin (jacoisback@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "include/Generator.h"
#include "include/version.h"
#include "include/Wave.h"
#include "include/Dat.h"

int Generator::ConvertFile(void)
{
   if (!this->Wav->waviFile.is_open() && !this->Dat->datiFile.is_open() )
      {
         cerr << "Abort. No input file defined!" <<endl;
         return 1;
      }
   if (this->Wav->waviFile.is_open() && this->Dat->datiFile.is_open() )
      {
         cerr << "Abort. Only one input file can be provided!" <<endl;
         return 2;
      }
   if (this->Wav->waviFile.is_open()) return this->ConvertWaveToDat();
   return this->ConvertDatToWave();
}


int Generator::ConvertDatToWave(void)
{
   string line;
   uint32_t samples=0;
   uint32_t bitrate;
   double prevtime=-1,ltime, lch1,lch2,deltatime=0;
   while (getline(this->Dat->datiFile,line))
   {
      if ((line.c_str())[0]!=';') //Process line only if not a comment
         {
            istringstream iss(line);
            if (!(iss >> ltime >> lch1 >> lch2)) { cerr <<  "Abort. Invalid file format."<<endl; return 3;}
            if (deltatime==0 && prevtime >0)
            {
               deltatime=ltime-prevtime;
               break;
            }
            prevtime=ltime;

         }
   }
   bitrate=1/deltatime;
   if (bitrate&1) bitrate++; //fix conversion error
   this->Wav->SetHeader(this->Channels,bitrate,this->Bits,0); //Set header need to update again at the end of file
   this->Dat->datiFile.seekg(0, ios::beg);
   while (getline(this->Dat->datiFile,line))
   {
      if ((line.c_str())[0]!=';') //Process line only if not a comment
         {
            istringstream iss(line);
            if (!(iss >> ltime >> lch1 >> lch2)) { cerr <<  "Abort. Invalid file format."<<endl; return 4; }
            this->Wav->WriteSampleData(lch1,lch2);
            samples++;
         }
   }
   this->Wav->SetHeader(this->Channels,bitrate,this->Bits,samples);
   return 0;
}

int Generator::ConvertWaveToDat(void)
{
   this->Wav->waviFile.seekg(0, ios::end);
   uint32_t filelength=this->Wav->waviFile.tellg();
   this->Wav->waviFile.seekg(0, ios::beg);
   this->Wav->waviFile.read((char *)&(this->Wav->wav_whdr),(streampos) sizeof(WaveFile::wav_hdr));
   if (strncmp((char *)this->Wav->wav_whdr.WAVE,"WAVE",4)) { cerr <<  "Abort. Invalid file format."<<endl; return 5; }
   if(this->Wav->wav_whdr.Subchunk2Size+sizeof(this->Wav->wav_whdr)!=filelength) { cerr <<  "Abort. Invalid file format."<<endl; return 6; }
   if(this->Wav->wav_whdr.NumOfChan>2||this->Wav->wav_whdr.NumOfChan<1) { cerr <<  "Abort. Invalid number of channels:"<<this->Wav->wav_whdr.NumOfChan<<endl ; return 7; }
   uint32_t Samples=this->Wav->wav_whdr.Subchunk2Size/(this->Wav->wav_whdr.NumOfChan*(((this->Wav->wav_whdr.bitsPerSample=Bits-1)>>3)+1));
   uint32_t OneDataLen=this->Wav->wav_whdr.blockAlign/this->Wav->wav_whdr.NumOfChan;
   double MaxSize=pow(2,this->Wav->wav_whdr.bitsPerSample)-1;
   this->Dat->SetHeader(this->Wav->wav_whdr.NumOfChan,this->Wav->wav_whdr.SamplesPerSec,this->Wav->wav_whdr.bitsPerSample,Samples);
   char buffer[10];
   int32_t ch1=0, ch2=0;
   int i;
   double curtime=0;
   while(Samples)
   {
      ch1=ch2=0;
      memset(buffer,0,sizeof(buffer));
      this->Wav->waviFile.read(&buffer[0],OneDataLen);
      for (i=1;i<=(int)OneDataLen;i++)
      {
         ch1=ch1<<8;
         ch1+=buffer[OneDataLen-i];
      }
      if (this->Wav->wav_whdr.NumOfChan==2)
      {
         memset(buffer,0,sizeof(buffer));
         this->Wav->waviFile.read(&buffer[0],OneDataLen);
         for (i=1;i<=(int)OneDataLen;i++)
         {
            ch2=ch2<<8;
            ch2+=buffer[OneDataLen-i];
         }
      }
      this->Dat->WriteSampleData(curtime,((double)ch1)/((double)MaxSize),((double)ch2)/((double)MaxSize));
      Samples--;
      curtime+=(1.0/(double)(this->Wav->wav_whdr.SamplesPerSec));
   }
   return 0;
}
