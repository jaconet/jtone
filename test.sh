#!/bin/bash
#jtone params
#-m mode: t for single tone, m for multi tone, s for sweep, n for noise
#-w waveform: s for sine, q for square, t for triangle, w for sow tooth, x for quasi-sine [s]
#-f minimum frequency in the range of 1 to 96000Hz [20]
#-F Maximum frequency in the range of 1 to 96000 Hz [20000]
#-d Number of frequency within an octave, from 1 to 12 [3]
#-r Samling rate, from 1000 to 192000 [48000](check audio card supported rates to avoid resampling)
#-t Duration in seconds from 0.01 to 600 [2]. You need to add 2*smooth time (#s parameter)
#-a amplitude between 0 and 1 [0.9]
#-g dB gain between 0 and -120 [~-1]
#-c channels: 0=left, 1=right, 2= both
#-s smooth intro and exit, seconds from 0.01 to 10 [0]


cd Test
echo
echo "Testing jtone"
echo
ercount=0
testcount=0

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test sine @1kHz "
echo
cmd="../Release/jtone -m t -w s -t 0.01 -r 100000 -f 1000 -F 5001 -c 0"
echo $cmd;echo;
$cmd  | ./testjtone 0 0   0 0    -3.02 -2.98    1000 1001   0.99 1   -1 -0.99   19 20   0.0099 0.01001 1.413 1.415
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test sine @1kHz gain -6db"
echo
cmd="../Release/jtone -m t -w s -t 0.01 -r 100000 -f 1000 -F 5001 -c 0  -g -6"
echo $cmd;echo;
$cmd | ./testjtone 0 0   0 0    -9.02 -8.98    1000 10001   0.49 0.51   -0.51 -0.49   19 20   0.0099 0.01001 1.413 1.415
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test triangle @2kHz "
echo
cmd="../Release/jtone -m t -w t -t 0.01 -r 100000 -f 2000 -F 5001 -c 0"
echo $cmd;echo;
$cmd  | ./testjtone 0 0   0 0    -4.8 -4.7    1000 1001   0.99 1   -1 -0.99   19 20   0.0099 0.01001 1.73 1.74
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test saw tooth @1kHz "
echo
cmd="../Release/jtone -m t -w x -t 0.01 -r 100000 -f 1000 -F 5001 -c 0"
echo $cmd;echo;
$cmd | ./testjtone 0 0   0 0    -3.02 -2.98    1000 1001   0.99 1   -1 -0.99   18 19   0.0099 0.01001 1.41 1.42
res=$?;
ercount=$[ercount+res];



#TEST
testcount=$[testcount+1];
echo
echo
echo "Test square @1kHz 48K Sampling"
echo
cmd="../Release/jtone -m t -w q -t 0.01  -f 1000 -F 5001 -c 0"
echo $cmd;echo;
$cmd   | ./testjtone 0 0   0 0    -0.01 0.01    480 481   0.99 1   -1 -0.99   8 9   0.0099 0.01001 0.999 1.001
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test defaults"
echo
cmd="../Release/jtone"
echo $cmd;echo;
$cmd  | ./testjtone  0 0   0 0    -3.02 -2.98    96000 96001   0.99 1   -1 -0.99   80 81   1.9999 2.0001  1.413 1.415
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test clipped sine @1kHz "
echo
cmd="../Release/jtone -m t -w s -t 0.1 -r 100000 -f 1000 -F 5001 -c 0 -g 6"
echo $cmd;echo;
$cmd  | ./testjtone 0 0   0 0    -1.74 -1.07    10000 10001   0.99 1   -1 -0.99   199 200   0.099 0.1001 1.125 1.135
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test multitone "
echo
cmd="../Release/jtone -m m -w s -t 0.1 -r 100000 -f 1000 -F 5001 -c 0"
echo $cmd;echo;
$cmd | ./testjtone 0 0   0 0    -12.1 -11.9    10000 10001   0.7 1   -1 -0.7   810 830   0.099 0.1001 3.0 3.1
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test Pulse Single @1kHz "
echo
cmd="../Release/jtone -m t -w s -p s -P 10 -Q 90  -t 1 -r 100000 -f 1000 -F 5001 -c 0"
echo $cmd;echo;
$cmd | ./testjtone 0 0   0 0    -23.02 -23.00    100000 100001   0.99 1   -1 -0.99   20 21   0.99 1.001 14.1 14.2
res=$?;
ercount=$[ercount+res];

#TEST
testcount=$[testcount+1];
echo
echo
echo "Test Pulse Repeated @1kHz "
echo
cmd="../Release/jtone -m t -w s -p r -P 20 -Q 80  -t 1 -r 100000 -f 1000 -F 5001 -c 0"
echo $cmd;echo;
$cmd | ./testjtone 0 0   0 0    -10.01 -9.99    100000 100001   0.99 1   -1 -0.99   408 410   0.99 1.001 3.15 3.17
res=$?;
ercount=$[ercount+res];



echo
if [ $ercount -ge 1 ];
then
   echo "Test failed with $ercount fails out of $[testcount*7] tests"
   exit $ercount
else
   echo "$[testcount*7] tests passed"
   exit 0
fi
